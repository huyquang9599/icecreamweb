import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { InformationComponent } from "./information/information.component";
import { UserlistComponent } from "./userlist/userlist.component";
import { RecipelistComponent } from "./recipelist/recipelist.component";
import { CustomerlistComponent } from "./customerlist/customerlist.component";
import { CInformationComponent } from "./c-information/c-information.component";
import { ReInformationComponent } from "./re-information/re-information.component";
import { OrderlistComponent } from "./orderlist/orderlist.component";
import { OrdersInformationComponent } from "./orders-information/orders-information.component";
import { IcecreamlistComponent } from "./icecreamlist/icecreamlist.component";
import { RecipeInfoComponent } from "./recipe-info/recipe-info.component";
import { ShoppingCartComponent } from "./shopping-cart/shopping-cart.component";
import { CustomerOrderListComponent } from "./customer-order-list/customer-order-list.component";
const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "information/:id", component: InformationComponent },
  { path: "userlist", component: UserlistComponent },
  { path: "customerlist", component: CustomerlistComponent },
  { path: "recipelist", component: RecipelistComponent },
  { path: "re_information/:id", component: ReInformationComponent },
  { path: "re_information", component: ReInformationComponent },
  { path: "c_information/:id", component: CInformationComponent },
  { path: "orderslist", component: OrderlistComponent },
  { path: "orders_information/:id", component: OrdersInformationComponent },
  { path: "icecreamlist", component: IcecreamlistComponent },
  { path: "recipeinfo/:id", component: RecipeInfoComponent },
  { path: "shoppingcart", component: ShoppingCartComponent },
  { path: "cusorderlist", component: CustomerOrderListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
