import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CInformationComponent } from './c-information.component';

describe('CInformationComponent', () => {
  let component: CInformationComponent;
  let fixture: ComponentFixture<CInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
