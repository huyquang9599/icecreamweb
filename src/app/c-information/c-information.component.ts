import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Customer } from "../model/Customer";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-c-information",
  templateUrl: "./c-information.component.html",
  styleUrls: ["./c-information.component.scss"],
})
export class CInformationComponent implements OnInit {
  customerid: number;
  customer: Customer;
  headers: any;
  gender: string;
  role: boolean;
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private cookieService: CookieService
  ) {}
  public selectedFile;
  imgURL: any = "../../assets/image/uploaded/noavatar.gif";
  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }

  ngOnInit(): void {
    this.customerid = Number(this.route.snapshot.paramMap.get("id"));
    this.role = !(this.cookieService.get("role") == "ROLE_ADMIN");
    this.api.getCustomerById(this.customerid).subscribe((resp) => {
      this.customer = resp;
      if (this.customer.gender == null) {
        this.gender = "Gender";
      } else if (this.customer.gender == "1") {
        this.gender = "Male";
      } else {
        this.gender = "Female";
      }
      console.log(this.customer);
    });

    this.api.getCustomerImageById(this.customerid).subscribe((res) => {
      this.imgURL = "data:image/jpeg;base64," + res;
    });
  }
  setGender(gender: string) {
    this.gender = gender;
  }
  saveProfile() {
    if (this.gender == "Male") {
      this.customer.gender = "true";
    } else {
      this.customer.gender = "false";
    }
    const uploadData = new FormData();
    uploadData.append("myFile", this.selectedFile);
    this.api.postUploadCustomerImage(uploadData, this.customerid).subscribe();
    this.api.postSaveCustomer(this.customer).subscribe();
    this._location.back();
  }

  onBack() {
    this._location.back();
  }
}
