import { Component, OnInit } from "@angular/core";
import { Recipe } from "../model/Recipe";
import { ApiService } from "../api.service";
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { async } from "@angular/core/testing";

@Component({
  selector: "app-recipelist",
  templateUrl: "./recipelist.component.html",
  styleUrls: ["./recipelist.component.scss"],
})
export class RecipelistComponent implements OnInit {
  in: String[] = [
    "id",
    "userid",
    "icecreamid",
    "title",
    "description",
    "price",
    "status",
    "viewcount",
    "image",
    "uploaddate",
  ];

  recipe: Array<Recipe> = [];
  totalPage: number;
  pageNum: number = 0;
  role: boolean;
  informationLink: string = "/information/";
  constructor(
    private api: ApiService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.informationLink += this.cookieService.get("id");
    this.role = this.cookieService.get("role") == "ROLE_USER";
    this.recipe = [];
    this.api.get5Recipe(this.pageNum).subscribe((resp) => {
      for (const data of resp) {
        if (data.status == "1") {
          data.status = "Active";
        } else {
          data.status = "Disable";
        }
        console.log(data);
        this.recipe.push(data);
      }
    });

    this.api.getTotalPage("/totalrecipepage").subscribe((resp) => {
      this.totalPage = resp;
    });
  }
  async deleteRecipe(id: number) {
    await this.api.deleteRecipeById(id);

    this.ngOnInit();
    alert("Recipe id " + id + " deleted!");
  }

  openProduct(id: number) {
    this.router.navigate(["/re_information", id]);
  }
  toLeftPage() {
    if (this.pageNum != 0) {
      this.pageNum--;
    }
    this.ngOnInit();
  }
  toRightPage() {
    if (this.pageNum < this.totalPage) {
      this.pageNum++;
    }
    this.ngOnInit();
  }

  addRecipe() {
    this.router.navigate(["re_information"]);
  }
}
