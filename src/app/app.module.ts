import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { InformationComponent } from "./information/information.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { UserlistComponent } from "./userlist/userlist.component";
import { CookieService } from "ngx-cookie-service";
import { CustomerlistComponent } from "./customerlist/customerlist.component";
import { CInformationComponent } from "./c-information/c-information.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RecipelistComponent } from "./recipelist/recipelist.component";
import { ReInformationComponent } from "./re-information/re-information.component";
import { OrderlistComponent } from "./orderlist/orderlist.component";
import { OrdersInformationComponent } from "./orders-information/orders-information.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IcecreamlistComponent } from './icecreamlist/icecreamlist.component';
import { RecipeInfoComponent } from './recipe-info/recipe-info.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CustomerOrderListComponent } from './customer-order-list/customer-order-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InformationComponent,
    RecipelistComponent,
    UserlistComponent,
    CustomerlistComponent,
    CInformationComponent,
    RecipelistComponent,
    ReInformationComponent,
    OrderlistComponent,
    OrdersInformationComponent,
    IcecreamlistComponent,
    RecipeInfoComponent,
    ShoppingCartComponent,
    CustomerOrderListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
})
export class AppModule {}
