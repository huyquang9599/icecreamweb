import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Location } from "@angular/common";
import { Recipe } from "../model/Recipe";
@Component({
  selector: "app-recipe-info",
  templateUrl: "./recipe-info.component.html",
  styleUrls: ["./recipe-info.component.scss"],
})
export class RecipeInfoComponent implements OnInit {
  recipeid: number;
  imgURL: any;
  recipe: Recipe;
  madeby: string;
  feedback: String[];
  cartNum: Array<string> = [];
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private cookieService: CookieService
  ) {}

  async ngOnInit() {
    if (this.cookieService.get("cartNum") != "") {
      this.cartNum = this.cookieService.get("cartNum").split(",");
    }

    this.recipeid = Number(this.route.snapshot.paramMap.get("id"));
    this.api.getRecipeImageById(this.recipeid).subscribe((res) => {
      if (res[0] != null) {
        this.imgURL = "data:image/jpeg;base64," + res;
      }
    });

    this.api.getRecipeById(this.recipeid).subscribe((resp) => {
      this.recipe = resp;
      this.api.getUserById(this.recipe.userid).subscribe((resp) => {
        this.madeby = resp.name;
      });
    });
    this.api.getFeebbackByRecipeId(this.recipeid).subscribe((resp) => {
      this.feedback = resp;
    });
  }
  addToCart() {
    if (this.cartNum.length < 5) {
      let b = true;
      for (let i = 0; i < this.cartNum.length; i++) {
        if (this.cartNum[i] == this.recipeid + "") {
          b = false;
        }
      }
      if (b) {
        this.cartNum.push(String(this.recipeid));
      } else {
        alert("This item has already in your cart");
      }

      console.log(this.cartNum);

      this.cookieService.set("cartNum", this.cartNum.toString());
    } else {
      alert("Only 5 items in cart at a time");
    }
    this._location.back();
  }
  onBack() {
    this._location.back();
  }
}
