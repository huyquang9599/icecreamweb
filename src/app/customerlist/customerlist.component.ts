import { Component, OnInit } from "@angular/core";
import { Customer } from "../model/Customer";
import { ApiService } from "../api.service";
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-customerlist",
  templateUrl: "./customerlist.component.html",
  styleUrls: ["./customerlist.component.scss"],
})
export class CustomerlistComponent implements OnInit {
  in: String[] = [
    "id",
    "name",
    "email",
    "phone",
    "dob",
    "address",
    "gender",
    "avatar",
    "status",
    "numofloginfailed",
  ];

  customer: Array<Customer> = [];
  totalPage: number;
  pageNum: number = 0;
  role: boolean;
  informationLink: string = "/information/";
  constructor(
    private api: ApiService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.informationLink += this.cookieService.get("id");
    this.role = this.cookieService.get("role") == "ROLE_ADMIN";
    console.log(this.role);

    this.customer = [];
    this.api.get5Customer(this.pageNum).subscribe((resp) => {
      for (const data of resp) {
        if (data.status == "1") {
          data.status = "Active";
        } else {
          data.status = "Locked";
        }
        if (data.gender == "1") {
          data.gender = "Male";
        } else {
          data.gender = "Female";
        }
        console.log(data);
        this.customer.push(data);
      }
    });

    this.api.getTotalPage("/totalcustomerpage").subscribe((resp) => {
      this.totalPage = resp;
    });
  }
  async deleteCustomer(id: number) {
    await this.api.deleteCustomerById(id);

    this.ngOnInit();
    alert("Customer id " + id + " deleted!");
  }

  openProfile(id: number) {
    this.router.navigate(["/c_information", id]);
  }
  toLeftPage() {
    if (this.pageNum != 0) {
      this.pageNum--;
    }
    this.ngOnInit();
  }
  toRightPage() {
    if (this.pageNum < this.totalPage) {
      this.pageNum++;
    }
    this.ngOnInit();
  }
}
