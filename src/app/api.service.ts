import { Injectable } from "@angular/core";
import { User } from "./model/User";
import { HttpClient, HttpResponse, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Observable, observable } from "rxjs";
import { Customer } from "./model/Customer";
import { Recipe } from "./model/Recipe";
import { Orders } from "./model/Orders";
import { OrdersDetails } from "./model/OrdersDetails";

const localUrl = "http://localhost:8080/icecream";

let options = {};
let loginHeader = new HttpHeaders({
  "Content-Type": "application/json",
});
@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient, private cookieService: CookieService) {}

  authen() {
    let token = this.cookieService.get("token");
    let headers = new HttpHeaders({
      Authorization: "Bearer " + token,
    });
    options = { headers: headers };
  }

  loginFail(username: string) {
    this.authen();
    return this.http.post(localUrl + "/loginfail/" + username, {
      headers: loginHeader,
    });
  }

  async deleteUserById(id: number) {
    this.authen();
    console.log(localUrl + "/deleteuserbyid/" + id);
    return await this.http
      .get(localUrl + "/deleteuserbyid/" + id, options)
      .toPromise();
  }
  getTotalPage(urlSuffix: string): Observable<number> {
    this.authen();
    console.log(localUrl + urlSuffix);
    return this.http.get<number>(localUrl + urlSuffix, options);
  }
  get5User(page: number): Observable<User[]> {
    this.authen();
    console.log(localUrl + "/get5user/" + page);
    return this.http.get<User[]>(localUrl + "/get5user/" + page, options);
  }
  getUserById(id: number): Observable<User> {
    this.authen();
    console.log(localUrl + "/finduserbyid/" + id);
    return this.http.get<User>(localUrl + "/finduserbyid/" + id, options);
  }
  postLogin(data: any) {
    return this.http.post(localUrl + "/login", JSON.stringify(data), {
      headers: loginHeader,
    });
  }
  postRegist(data: any) {
    return this.http.post(localUrl + "/adduser", JSON.stringify(data), {
      headers: loginHeader,
    });
  }

  get5Customer(page: number): Observable<Customer[]> {
    this.authen();
    console.log(localUrl + "/get5customer/" + page);
    return this.http.get<Customer[]>(
      localUrl + "/get5customer/" + page,
      options
    );
  }
  async deleteCustomerById(id: number) {
    this.authen();
    console.log(localUrl + "/deletecustomerbyid/" + id);
    return await this.http
      .get(localUrl + "/deletecustomerbyid/" + id, options)
      .toPromise();
  }

  getRoleByUsername(username: string): Observable<any> {
    this.authen();
    console.log(localUrl + "/getrolebyusername/" + username);
    return this.http.get<any>(
      localUrl + "/getrolebyusername/" + username,
      options
    );
  }

  getCustomerById(id: number): Observable<Customer> {
    this.authen();
    console.log(localUrl + "/findcustomerbyid/" + id);
    return this.http.get<Customer>(
      localUrl + "/findcustomerbyid/" + id,
      options
    );
  }
  postSaveUser(user: User) {
    this.authen();
    console.log(localUrl + "/updateuser");
    return this.http.post(localUrl + "/updateuser", user, options);
  }

  postSaveCustomer(customer: Customer) {
    this.authen();
    console.log(localUrl + "/updatecustomer");
    return this.http.post(localUrl + "/updatecustomer", customer, options);
  }

  get5Recipe(page: number): Observable<Recipe[]> {
    this.authen();
    console.log(localUrl + "/get5recipe/" + page);
    return this.http.get<Recipe[]>(localUrl + "/get5recipe/" + page, options);
  }

  async deleteRecipeById(id: number) {
    this.authen();
    console.log(localUrl + "/deleterecipebyid/" + id);
    return await this.http
      .get(localUrl + "/deleterecipebyid/" + id, options)
      .toPromise();
  }

  getRecipeById(id: number): Observable<Recipe> {
    this.authen();
    console.log(localUrl + "/findrecipebyid/" + id);
    return this.http.get<Recipe>(localUrl + "/findrecipebyid/" + id, options);
  }

  getAllUser(): Observable<User[]> {
    this.authen();
    console.log(localUrl + "/alluser");
    return this.http.get<User[]>(localUrl + "/alluser", options);
  }

  postSaveRecipe(recipe: Recipe) {
    this.authen();
    console.log(localUrl + "/updaterecipe");
    return this.http.post(localUrl + "/updaterecipe", recipe, options);
  }

  get5Orders(page: number): Observable<Orders[]> {
    this.authen();
    console.log(localUrl + "/get5orders/" + page);
    return this.http.get<Orders[]>(localUrl + "/get5orders/" + page, options);
  }

  async deleteOrdersById(id: number) {
    this.authen();
    console.log(localUrl + "/deleteordersbyid/" + id);
    return await this.http
      .get(localUrl + "/deleteordersbyid/" + id, options)
      .toPromise();
  }

  getOrdersDetailsById(id: number): Observable<OrdersDetails[]> {
    this.authen();
    console.log(localUrl + "/findorderdetailsbyid/" + id);
    return this.http.get<OrdersDetails[]>(
      localUrl + "/findorderdetailsbyid/" + id,
      options
    );
  }

  getOrdersById(id: number): Observable<Orders> {
    this.authen();
    console.log(localUrl + "/findorderbyid/" + id);
    return this.http.get<Orders>(localUrl + "/findorderbyid/" + id, options);
  }

  get5OrdersByCustomerId(id: number, page: number): Observable<Orders[]> {
    this.authen();
    console.log(localUrl + "/findorderbycustomerid/" + id);
    return this.http.get<Orders[]>(
      localUrl + "/findorderbycustomerid/" + id + "/" + page,
      options
    );
  }

  postSaveOrderStatus(id: number, status: number) {
    this.authen();
    console.log(localUrl + "/updateordersstatusbyid/" + id + "/" + status);
    return this.http.get(
      localUrl + "/updateordersstatusbyid/" + id + "/" + status,
      options
    );
  }

  postUploadUserImage(uploadData: any, id: number) {
    this.authen();
    return this.http.post(
      localUrl + "/uploaduserimagebyid/" + id,
      uploadData,
      options
    );
  }

  getUserImageById(id: number): Observable<any> {
    this.authen();
    console.log(localUrl + "/getuserimagebyid/" + id);
    return this.http.get<any>(localUrl + "/getuserimagebyid/" + id, options);
  }

  postUploadCustomerImage(uploadData: any, id: number) {
    this.authen();
    return this.http.post(
      localUrl + "/uploadcusimagebyid/" + id,
      uploadData,
      options
    );
  }

  getCustomerImageById(id: number): Observable<any> {
    this.authen();
    console.log(localUrl + "/getcusimagebyid/" + id);
    return this.http.get<any>(localUrl + "/getcusimagebyid/" + id, options);
  }

  postUploadRecipeImage(uploadData: any, id: number) {
    this.authen();
    return this.http.post(
      localUrl + "/uploadrecipeimagebyid/" + id,
      uploadData,
      options
    );
  }

  getRecipeImageById(id: number): Observable<any> {
    this.authen();
    console.log(localUrl + "/getrecipeimagebyid/" + id);
    return this.http.get<any>(localUrl + "/getrecipeimagebyid/" + id, options);
  }

  getAllRecipe(): Observable<Recipe[]> {
    this.authen();
    console.log(localUrl + "/allrecipe");
    return this.http.get<Recipe[]>(localUrl + "/allrecipe", options);
  }

  getFeebbackByRecipeId(recipeid: number): Observable<String[]> {
    this.authen();
    console.log(localUrl + "/getfeedbackbyrecipeid/" + recipeid);
    return this.http.get<String[]>(
      localUrl + "/getfeedbackbyrecipeid/" + recipeid,
      options
    );
  }

  addViewCount(id: number) {
    this.authen();
    return this.http.get(localUrl + "/increaserecipeviewcount/" + id, options);
  }

  async postSaveOrders(orders: Orders) {
    this.authen();
    console.log(localUrl + "/addorders");
    return await this.http
      .post(localUrl + "/addorders", orders, options)
      .toPromise();
  }

  postSaveOrderDetails(orderdetails: OrdersDetails) {
    this.authen();
    console.log(localUrl + "/addorderdetails");
    return this.http.post(localUrl + "/addorderdetails", orderdetails, options);
  }
}
