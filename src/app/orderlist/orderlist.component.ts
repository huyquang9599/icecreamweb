import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Orders } from "../model/Orders";
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
  stagger,
  group,
  // ...
} from "@angular/animations";
@Component({
  selector: "app-orderlist",
  templateUrl: "./orderlist.component.html",
  styleUrls: ["./orderlist.component.scss"],
})
export class OrderlistComponent implements OnInit {
  in: String[] = [
    "id",
    "customerid",
    "paymentoption",
    "paymentid",
    "createdate",
    "deliverydetail",
    "notes",
    "status",
  ];

  orders: Array<Orders> = [];
  totalPage: number;
  pageNum: number = 0;
  role: boolean;
  informationLink: string = "/information/";
  constructor(
    private api: ApiService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.informationLink += this.cookieService.get("id");
    this.role = this.cookieService.get("role") == "ROLE_ADMIN";
    this.orders = [];
    this.api.get5Orders(this.pageNum).subscribe((resp) => {
      for (const data of resp) {
        if (data.status == "0") {
          data.status = "New";
        } else if (data.status == "1") {
          data.status = "InProgress";
        } else if (data.status == "2") {
          data.status = "Done";
        } else {
          data.status = "Rejected";
        }
        console.log(data);
        this.orders.push(data);
      }
    });

    this.api.getTotalPage("/totalorderspage").subscribe((resp) => {
      this.totalPage = resp;
    });
  }

  async deleteOrders(id: number) {
    await this.api.deleteOrdersById(id);

    this.ngOnInit();

    alert("Orders id " + id + " deleted!");
  }

  openOrders(orderid: number) {
    this.router.navigate(["/orders_information", orderid]);
  }
  toLeftPage() {
    if (this.pageNum != 0) {
      this.pageNum--;
    }

    this.ngOnInit();
  }
  toRightPage() {
    if (this.pageNum < this.totalPage) {
      this.pageNum++;
    }
    this.ngOnInit();
  }
}
