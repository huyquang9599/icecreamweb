import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Location, DatePipe } from "@angular/common";
import { Orders } from "../model/Orders";
import { OrdersDetails } from "../model/OrdersDetails";

@Component({
  selector: "app-shopping-cart",
  templateUrl: "./shopping-cart.component.html",
  styleUrls: ["./shopping-cart.component.scss"],
  providers: [DatePipe],
})
export class ShoppingCartComponent implements OnInit {
  in: String[] = ["Recipe", "Quantity", "Price"];

  informationLink: string = "/information/";
  cartNum: Array<string> = [];
  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private _location: Location,
    private datePipe: DatePipe
  ) {}
  orderdetails: OrdersDetails = {
    orderid: null,
    recipeid: null,
    quantity: null,
    price: null,
    note: null,
  };
  orders: Orders = {
    id: null,
    customerid: null,
    paymentoption: "null",
    paymentid: "1",
    createdate: this.datePipe.transform(new Date(), "yyyy-MM-dd"),
    deliverydetail: null,
    note: null,
    status: "0",
  };
  recipe: Array<string> = [];
  quantity: Array<number> = [];
  price: Array<number> = [];
  ngOnInit(): void {
    console.log(this.cookieService.get("cartNum").toString());

    this.cartNum = this.cookieService.get("cartNum").split(",");

    this.cartNum.forEach((e) => {
      let recipeid = Number(e.toString());
      this.api.getRecipeById(recipeid).subscribe((resp) => {
        console.log(resp);

        this.quantity.push(1);
        this.price.push(resp.price);
        this.recipe.push(resp.title);
      });
    });

    this.informationLink += this.cookieService.get("id");
  }
  removeOrder(no: number) {
    this.cartNum.splice(no, 1);
    this.cookieService.set("cartNum", this.cartNum.toString());
    this.recipe.splice(no, 1);
    this.quantity.splice(no, 1);
    this.price.splice(no, 1);
  }

  removeAll() {
    this.cookieService.delete("cartNum");

    this._location.back();
  }

  saveOrders() {
    this.orders.customerid = this.cookieService.get("id");
    this.api.postSaveOrders(this.orders).then((e) => {
      for (let i = 0; i < this.cartNum.length; i++) {
        this.orderdetails.recipeid = Number(this.cartNum[i]);
        this.orderdetails.quantity = Number(this.quantity[i]);
        this.orderdetails.price = Number(this.price[i]);

        this.api.postSaveOrderDetails(this.orderdetails).subscribe();
      }
    });
    this.removeAll();
  }

  cancel() {
    this._location.back();
  }
}
