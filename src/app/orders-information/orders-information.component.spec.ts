import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersInformationComponent } from './orders-information.component';

describe('OrdersInformationComponent', () => {
  let component: OrdersInformationComponent;
  let fixture: ComponentFixture<OrdersInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
