import { Component, OnInit } from "@angular/core";
import { OrdersDetails } from "../model/OrdersDetails";
import { ApiService } from "../api.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Location } from "@angular/common";

@Component({
  selector: "app-orders-information",
  templateUrl: "./orders-information.component.html",
  styleUrls: ["./orders-information.component.scss"],
})
export class OrdersInformationComponent implements OnInit {
  in: String[] = ["recipeid", "quantity", "price", "notes"];
  orderid: any;
  ordersdetails: Array<OrdersDetails> = [];
  totalPage: number;
  pageNum: number = 0;
  role: boolean;
  informationLink: string = "/information/";
  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private _location: Location
  ) {}

  statusString: String[] = ["New", "InProgress", "Done", "Rejected"];
  statusNum: number;
  status: string;
  ngOnInit(): void {
    this.orderid = Number(this.route.snapshot.paramMap.get("id"));
    this.informationLink += this.cookieService.get("id");
    this.role = this.cookieService.get("role") == "ROLE_ADMIN";
    this.ordersdetails = [];
    this.api.getOrdersById(this.orderid).subscribe((resp) => {
      this.status = this.statusString[resp.status];
      this.statusNum = Number(resp.status);
    });
    this.api.getOrdersDetailsById(this.orderid).subscribe((resp) => {
      for (const data of resp) {
        console.log(data);

        this.ordersdetails.push(data);
      }
    });
  }

  setStatus(status: string, no: any) {
    this.statusNum = no;
    this.status = status;
  }

  saveStatus() {
    this.api
      .postSaveOrderStatus(this.orderid, this.statusNum)
      .subscribe((resp) => {
        console.log(resp[status]);
      });
    this._location.back();
  }

  cancel() {
    this._location.back();
  }
}
