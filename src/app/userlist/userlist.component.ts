import { Component, OnInit } from "@angular/core";
import { User } from "../model/User";
import { ApiService } from "../api.service";
import { Router } from "@angular/router";
import { NgStyle } from "@angular/common";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-userlist",
  templateUrl: "./userlist.component.html",
  styleUrls: ["./userlist.component.scss"],
})
export class UserlistComponent implements OnInit {
  in: String[] = ["id", "name", "status", "phonenumber", "avatar"];
  user: Array<User> = [];
  totalPage: number;
  pageNum: number = 0;
  role: boolean;
  informationLink: string = "/information/";
  //style:any[]=["width:10px;","width:10px;","width:10px;","width:10px;",width:10px;];
  constructor(
    private api: ApiService,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.informationLink += this.cookieService.get("id");
    this.role = this.cookieService.get("role") == "ROLE_ADMIN";
    this.user = [];
    this.api.get5User(this.pageNum).subscribe((resp) => {
      for (const data of resp) {
        if (data.status == "1") {
          data.status = "Active";
        } else {
          data.status = "Locked";
        }
        this.user.push(data);
      }
    });

    this.api.getTotalPage("/totaluserpage").subscribe((resp) => {
      this.totalPage = resp;
    });
  }

  async deleteUser(id: number) {
    await this.api.deleteUserById(id);

    this.ngOnInit();
    alert("User id " + id + " deleted!");
  }

  openProfile(id: string) {
    this.router.navigate(["/information", id]);
  }
  toLeftPage() {
    if (this.pageNum != 0) {
      this.pageNum--;
    }
    this.ngOnInit();
  }
  toRightPage() {
    if (this.pageNum < this.totalPage) {
      this.pageNum++;
    }
    this.ngOnInit();
  }
}
