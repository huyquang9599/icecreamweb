import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcecreamlistComponent } from './icecreamlist.component';

describe('IcecreamlistComponent', () => {
  let component: IcecreamlistComponent;
  let fixture: ComponentFixture<IcecreamlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcecreamlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcecreamlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
