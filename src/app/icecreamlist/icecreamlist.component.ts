import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

import { Location } from "@angular/common";
import { Recipe } from "../model/Recipe";
@Component({
  selector: "app-icecreamlist",
  templateUrl: "./icecreamlist.component.html",
  styleUrls: ["./icecreamlist.component.scss"],
})
export class IcecreamlistComponent implements OnInit {
  recipelist: Array<Recipe> = [];
  informationLink: string = "/c_information/";
  cartNum: Array<string> = [];
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.recipelist = [];
    if (this.cookieService.get("cartNum") != "") {
      this.cartNum = this.cookieService.get("cartNum").split(",");
    }

    this.informationLink += this.cookieService.get("id");
    this.api.getAllRecipe().subscribe((resp) => {
      this.recipelist = resp;
    });
  }
  addToCart(id: string) {
    if (this.cartNum.length < 5) {
      let b = true;
      for (let i = 0; i < this.cartNum.length; i++) {
        if (this.cartNum[i] == id) {
          b = false;
        }
      }
      if (b) {
        this.cartNum.push(id);
      } else {
        alert("This item has already in your cart");
      }

      console.log(this.cartNum);

      this.cookieService.set("cartNum", this.cartNum.toString());
    } else {
      alert("Only 5 items in cart at a time");
    }
  }

  recipeInfo(id: number) {
    this.api.addViewCount(id).subscribe();
    this.router.navigate(["/recipeinfo", id]);
  }
  openCart() {
    if (this.cartNum.length < 1) {
      alert("Empty cart");
    } else {
      this.router.navigate(["/shoppingcart"]);
    }
  }
}
