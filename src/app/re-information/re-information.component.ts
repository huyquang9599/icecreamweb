import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Recipe } from "../model/Recipe";
import { User } from "../model/User";

import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-re-information",
  templateUrl: "./re-information.component.html",
  styleUrls: ["./re-information.component.scss"],
})
export class ReInformationComponent implements OnInit {
  recipeid: number;
  recipeJson: Recipe = {
    id: null,
    userid: null,
    title: null,
    description: null,
    price: null,
    status: null,
    viewcount: null,
    image: null,
    uploaddate: null,
  };
  headers: any;
  userJson: any = {};
  user: Array<User> = [];
  role: boolean;

  public selectedFile;
  imgURL: any = "../../assets/image/no_image.png";
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService
  ) {}

  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }

  ngOnInit(): void {
    this.role = this.cookieService.get("role") == "ROLE_USER";
    this.recipeid = Number(this.route.snapshot.paramMap.get("id"));
    if (this.route.snapshot.paramMap.get("id") != null) {
      this.api.getRecipeById(this.recipeid).subscribe((resp) => {
        this.recipeJson = resp;
        this.api.getUserById(this.recipeJson.userid).subscribe((resp) => {
          this.userJson.name = resp.name;
          this.userJson.id = resp.id;
        });
      });

      this.api.getRecipeImageById(this.recipeid).subscribe((res) => {
        if (res[0] != null) {
          this.imgURL = "data:image/jpeg;base64," + res;
        }
      });

      this.api.getAllUser().subscribe((resp) => {
        this.user = resp;
      });
    } else {
      this.recipeJson.userid = Number(this.cookieService.get("id"));

      this.api.getUserById(this.recipeJson.userid).subscribe((resp) => {
        this.userJson.name = resp.name;
        this.recipeJson.userid = resp.id;
      });
    }
  }
  setUsername(id: number, name: string) {
    this.userJson.name = name;
    this.recipeJson.userid = id;
  }

  saveProfile() {
    console.log(this.recipeJson);
    this.recipeJson.viewcount = 0;
    this.api.postSaveRecipe(this.recipeJson).subscribe();
    const uploadData = new FormData();
    uploadData.append("myFile", this.selectedFile);
    this.api.postUploadRecipeImage(uploadData, this.recipeJson.id).subscribe();
    this.router.navigate(["/recipelist"]);
  }

  onBack() {
    this.router.navigate(["/recipelist"]);
  }
}
