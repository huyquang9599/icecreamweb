import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReInformationComponent } from './re-information.component';

describe('ReInformationComponent', () => {
  let component: ReInformationComponent;
  let fixture: ComponentFixture<ReInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
