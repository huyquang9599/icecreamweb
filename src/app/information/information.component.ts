import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { User } from "../model/User";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Location } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-information",
  templateUrl: "./information.component.html",
  styleUrls: ["./information.component.scss"],
})
export class InformationComponent implements OnInit {
  userid: number;
  user: User;
  headers: any;
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private cookieService: CookieService
  ) {}

  public selectedFile;
  imgURL: any = "../../assets/image/uploaded/noavatar.gif";
  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }

  ngOnInit(): void {
    this.userid = Number(this.route.snapshot.paramMap.get("id"));

    this.api.getUserById(this.userid).subscribe((resp) => {
      this.user = resp;
    });

    this.api.getUserImageById(this.userid).subscribe((res) => {
      if (res[0] != null) {
        this.imgURL = "data:image/jpeg;base64," + res;
      }
    });
  }

  saveProfile() {
    this.api.postSaveUser(this.user).subscribe();
    const uploadData = new FormData();
    uploadData.append("myFile", this.selectedFile);
    this.api.postUploadUserImage(uploadData, this.userid).subscribe();
    console.log(this.user);
    this.router.navigate(["/userlist"]);
  }

  backClicked() {
    this._location.back();
  }
}
