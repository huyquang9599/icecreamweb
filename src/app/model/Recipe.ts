export interface Recipe {
  id: number;
  userid: number;
  title: string;
  description: string;
  price: number;
  status: string;
  viewcount: number;
  image: string;
  uploaddate: Date;
}
