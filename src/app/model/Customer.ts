export interface Customer {
  id: number;
  name: string;
  email: string;
  password: string;
  phone: string;
  dob: Date;
  address: string;
  gender: string;
  avatar: string;
  status: string;
  numofloginfail: number;
}
