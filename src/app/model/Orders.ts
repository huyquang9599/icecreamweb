export interface Orders {
  id: number;
  customerid: string;
  paymentoption: string;
  paymentid: string;
  createdate: string;
  deliverydetail: string;
  note: string;
  status: string;
}
