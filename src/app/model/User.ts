export interface User {
  id: number;
  name: string;
  status: string;
  password: string;
  phonenumber: string;
  avatar: string;
}
