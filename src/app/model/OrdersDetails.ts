export interface OrdersDetails {
  orderid: number;
  recipeid: number;
  quantity: number;
  price: number;
  note: string;
}
