import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { FormBuilder } from "@angular/forms";
import * as jwt_decode from "jwt-decode";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm;

  data: any;
  constructor(
    private api: ApiService,
    private cookieService: CookieService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      _username: "",
      _password: "",
    });
  }
  getDecodedAccessToken(token: string): any {
    return;
  }
  ngOnInit(): void {}

  loginsubmit(logindata) {
    this.data = {
      username: logindata._username,
      password: logindata._password,
    };
    this.api.postLogin(this.data).subscribe(
      (res) => {
        this.cookieService.deleteAll();
        let token = jwt_decode(res["token"]);

        this.cookieService.set("token", res["token"]);
        this.cookieService.set("role", token.role);
        this.cookieService.set("id", token.id);
        if (token.role == "ROLE_CUSTOMER") {
          this.router.navigate(["/icecreamlist"]);
        } else {
          this.router.navigate(["/userlist"]);
        }

        this.loginForm.reset();
      },
      (err) => {
        this.api.loginFail(this.data.username);
        alert("Wrong username or password");
      }
    );
  }
  regist(logindata) {
    this.data = {
      name: logindata._username,
      password: logindata._password,
    };
    if (this.data.name != "" && this.data.password != "") {
      this.api.postRegist(this.data).subscribe(
        (res) => {
          alert("Registed");
        },
        (err) => {
          alert("Username existed!");
        }
      );
    } else {
      alert("blank field!!!");
    }
  }

  forgotPass() {
    alert("HAHAHA, just create a new one, ez~");
  }
}
