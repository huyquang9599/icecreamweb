import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { OrdersDetails } from "../model/OrdersDetails";
import { ApiService } from "../api.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Orders } from "../model/Orders";
@Component({
  selector: "app-customer-order-list",
  templateUrl: "./customer-order-list.component.html",
  styleUrls: ["./customer-order-list.component.scss"],
})
export class CustomerOrderListComponent implements OnInit {
  in: String[] = ["Id", "Created Date", "Total", "Status"];
  customerid: number;
  orders: Array<Orders>;
  orderdetails: Array<OrdersDetails> = [];
  total: Array<number> = [];
  totalPage: number;
  pageNum: number = 0;

  informationLink: string = "/information/";
  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private _location: Location
  ) {}

  statusString: String[] = ["New", "InProgress", "Done", "Rejected"];
  statusNum: number;
  status: string;
  ngOnInit(): void {
    this.customerid = Number(this.cookieService.get("id"));
    this.informationLink += this.cookieService.get("id");

    this.orders = [];
    this.api
      .get5OrdersByCustomerId(this.customerid, this.pageNum)
      .toPromise()
      .then((resp) => {
        resp.forEach((e) => {
          this.orders.push(e);
          let ito = 0;
          this.api.getOrdersDetailsById(e.id).subscribe((resp2) => {
            resp2.forEach((e2) => {
              ito += e2.price;
            });
            this.total.push(ito);
          });
        });
      });

    this.api
      .getTotalPage("/totalorderspage")
      .subscribe((resp) => (this.totalPage = resp));
  }
  toLeftPage() {
    if (this.pageNum != 0) {
      this.pageNum--;
    }
    this.ngOnInit();
  }
  toRightPage() {
    if (this.pageNum < this.totalPage) {
      this.pageNum++;
    }
    this.ngOnInit();
  }
}
